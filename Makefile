# contrib/email/Makefile

MODULE_big = email
OBJS = email.o 

EXTENSION = email
DATA = email--0.1.sql

REGRESS = email

PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)
